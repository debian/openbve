#!/usr/bin/make -f
export DH_VERBOSE=1
BUILDDIRS = openBVE/OpenBve openBVE/OpenBveApi \
 openBVE/OpenBveAts \
 openBVE/Sound.Flac openBVE/Sound.RiffWave \
 openBVE/Texture.Ace openBVE/Texture.BmpGifJpegPngTiff
TARGET = $(CURDIR)/debian/openbve
EXEC_TARGET = $(TARGET)/usr/lib/openbve
EXEC_PLUGIN_TARGET = $(EXEC_TARGET)/Plugins
#DEBUG_CONFIGURATION=Debug
DEBUG_CONFIGURATION=Release

%:
	dh $@ --with=cli

override_dh_clideps:
	dh_clideps --exclude-moduleref=AtsPluginProxy --exclude-moduleref=AtsPluginProxy.dll

override_dh_auto_build:
	for builddir in $(BUILDDIRS); do \
	  (cd $$builddir && xbuild /p:Configuration=$(DEBUG_CONFIGURATION) *.csproj) || exit 1; \
	done

override_dh_clean:
	dh_clean
	rm -rf $(TARGET)
	for builddir in $(BUILDDIRS); do \
	  (cd $$builddir && xbuild /t:Clean *.csproj); \
	  rm -rf $$builddir/bin $$builddir/obj; \
	done

override_dh_install:
	dh_install
	# The wrapped is installed are here because the permissions need to be set
	install -D -m 755 -T $(CURDIR)/debian/openbve.wrapper $(TARGET)/usr/games/openbve
	# Moved creation from 'debian/dirs' to 'debian/rules' per sponsorship feedback	
	install -d $(EXEC_TARGET) $(EXEC_PLUGIN_TARGET)
	# NOTE: these are here, rather than in debian/*.install because it is not
	# possible to switch between the setting of \$(DEBUG_CONFIGURATION) otherwise
	install -m 755 $(CURDIR)/openBVE/OpenBve/bin/$(DEBUG_CONFIGURATION)/OpenBve.exe $(EXEC_TARGET)
	#install -m 644 $(CURDIR)/openBVE/OpenBve/bin/$(DEBUG_CONFIGURATION)/OpenBve.exe.mdb $(EXEC_TARGET)
	install -m 755 $(CURDIR)/openBVE/OpenBveApi/bin/$(DEBUG_CONFIGURATION)/OpenBveApi.dll $(EXEC_TARGET)
	install -m 644 $(CURDIR)/openBVE/OpenBveApi/bin/$(DEBUG_CONFIGURATION)/OpenBveApi.dll.mdb $(EXEC_TARGET)
	install -m 755 $(CURDIR)/openBVE/OpenBveAts/bin/$(DEBUG_CONFIGURATION)/OpenBveAts.dll $(EXEC_PLUGIN_TARGET)
	install -m 755 $(CURDIR)/openBVE/Sound.Flac/bin/$(DEBUG_CONFIGURATION)/Sound.Flac.dll $(EXEC_PLUGIN_TARGET)
	install -m 755 $(CURDIR)/openBVE/Sound.RiffWave/bin/$(DEBUG_CONFIGURATION)/Sound.RiffWave.dll $(EXEC_PLUGIN_TARGET)
	install -m 755 $(CURDIR)/openBVE/Texture.Ace/bin/$(DEBUG_CONFIGURATION)/Texture.Ace.dll $(EXEC_PLUGIN_TARGET)
	install -m 755 $(CURDIR)/openBVE/Texture.BmpGifJpegPngTiff/bin/$(DEBUG_CONFIGURATION)/Texture.BmpGifJpegPngTiff.dll $(EXEC_PLUGIN_TARGET)
	dh_installchangelogs debian/changelog.html

# For running manually
fetch-html:
	# The wget is two-step; the mv will only occur if wget succeeded and exited cleanly
	# One could use 'wget -O' but if the website is down or the transfer interrupted then
	# the existing copy is corrupted.  This happened and lead to the current method
	wget http://trainsimframework.org/changelog.html && mv changelog.html.1 changelog.html
	#wget http://trainsimframework.org/releasenotes.html && mv releasenotes.html.1 releasenotes.html
	wget http://trainsimframework.org/credits.html && mv credits.html.1 credits.html
